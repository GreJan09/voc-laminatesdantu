
// init Swiper:
const swiper = new Swiper('.swiper-container', {
        effect: "coverflow",
        // paginationClickable: true,
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 1,
        loop: true,
        // pagination: '.swiper-pagination',
        // pagination: '.swiper-pagination',
        // paginationClickable: true,
        // pagination: {
        //   el: '.swiper-pagination',
        //   type: 'bullets',
        //   clickable: true
        // },

        // clickableClass: '.swiper-pagination',
        // pagination: {
        //   el: '.swiper-pagination',
        //   clickable: true,
        // },
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        breakpoints: {
          768 : {
            slidesPerView: 1,
            spaceBetween: 20
          }
        },
        breakpoints: {
          1124 : {
            slidesPerView: 3,
            spaceBetween: 5
          }
        }
});
