
var beforeAfterSlide = document.querySelectorAll("#before-after-slide");

beforeAfterSlide.forEach(item => {
  bearSlider(item);
  item.addEventListener("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  item.addEventListener("mouseenter", function() {
    if(item.parentElement.getAttribute('data-type') === 'active' || item.getAttribute('data-type') === 'slide-active') {
    customSliding(item);
      return;
    }
  })
});
    // new BeerSlider( item, {
    //   // start value
    //   start: '50',
    //   // prefix 
    //   prefix: 'beer'
    // });
// )


function bearSlider(item) {
  new BeerSlider( item, {
    // start value
    start: '50',
    // prefix 
    prefix: 'beer'
  });
}


function customSliding(item) {
  // item.addEventListener("mouseenter", function(e) {
      item.addEventListener("mousemove", function (event) {
        var rect = event.target.getBoundingClientRect();
        var x = event.clientX - rect.left; //x position within the element.
        item.children[3].style.left = `${x}px`;
        item.children[1].style.width = `${x}px`;
      });
      item.addEventListener("mouseleave", function(e) {
        item.children[3].style.left = '50%';
        item.children[1].style.width = '50%';
      });
  // })

  // let x1 = null;
  // function handleTouchStart(event) {
  //   const firstTouch = event.touches[0];
  //   x1 = firstTouch.clientX
  // }

  // function handleTouchMove(event) {
  //   if (!x1) {
  //       return false;
  //   }
  //   let x2 = event.touches[0].clientX;
  //   let xDiff = x2 - x1;
  //   if (xDiff > 0) {
  //       moveSliderLeft();
  //   } else {
  //       moveSliderRigth();
  //   }
}




